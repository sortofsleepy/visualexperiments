This is just a simple experiment messing around with particle systems. 
Particles are floating around and when they get close enough to one another, 
a line should connect particles together. 

I was using this as a opportunity to try out Pixi.js as well as just get more 
practice building particle systems. It needs a little work but I was happy to just get the idea
working. 

Around this time I was also very much intersted in Haxe, which is a interesting 
language that can compile code for a variety of platforms including HTML5. I've since moved onto 
just using ES6 style javascript since tools like BabelJS are available but it's definetly worth 
future investigation for future projects.

You should be able to just run the index.html file in your browser without the use of a server.
You'll have to add some query paramters though so the final url should look something like 

...<whatever url>?bgcolor=0x333333&particlecolor=0xffffff/

The reason for this being was that I originally wanted to be able to embed this and wanted the ability to 
customize some parameters depending on where I was embeding this.