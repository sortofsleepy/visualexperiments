
import 'THREE/THREE.dart';
import 'dart:typed_data';
import 'dart:js';
import 'dart:math';
import 'THREE/Particle.dart';
import 'package:vector_math/vector_math.dart';

class System{

    //typed arrays for stuffs.
    Float32List positions;
    Float32List colors;
    Float32List sizes;

    //number of particles
    int num_particles;

    //radius
    var radius = 200;

    Random random = new Random();

    //color object
    JsObject color = Three.make("Color");

    //buffer geometry
    JsObject geometry = Three.make("BufferGeometry");

    //the equivalent mesh for a particle system
    JsObject pointCloud;

    //shader material
    Material material;

    var Float32Array = context["Float32Array"];

    var particles = [];
    System([int num_particles = 1000]){
        this.num_particles = num_particles;
        Console.log(Float32Array);
        positions = new Float32List(this.num_particles * 3);
        colors = new Float32List(this.num_particles * 3);
        sizes = new Float32List(this.num_particles * 3);


        //set initial positions
        for(var v = 0;v<this.num_particles;++v){
            Particle p = new Particle();


            sizes[ v ] = 20.0;

            positions[ v * 3 + 0 ] = (random.nextDouble() * 2 - 1 ) * radius;
            positions[ v * 3 + 1 ] = ( random.nextDouble() * 2 - 1 ) * radius;
            positions[ v * 3 + 2 ] = ( random.nextDouble() * 2 - 1 ) * radius;

            color.callMethod("setHSL", [v / num_particles, 1.0, 0.5 ]);


            if(color["r"] is int){
                color["r"] = random.nextDouble();
            }
            if(color["g"] is int){
                color["g"] = random.nextDouble();
            }
            if(color["b"] is int){
                color["b"] = random.nextDouble();
            }

            colors[ v * 3 + 0 ] = color["r"];
            colors[ v * 3 + 1 ] = color["g"];
            colors[ v * 3 + 2 ] = color["b"];
        }

        //need to convert to a actual Float32Array because just putting in the list creates a ArrayBuffer
        var positionAttribute = new JsObject(context["THREE"]["BufferAttribute"],[new JsObject(context["Float32Array"],[positions]),3]);
        var colorAttribute = new JsObject(context["THREE"]["BufferAttribute"],[new JsObject(context["Float32Array"],[colors]),3]);
        var sizeAttribute = new JsObject(context["THREE"]["BufferAttribute"],[new JsObject(context["Float32Array"],[sizes]),3]);

        geometry.callMethod("addAttribute",['position',positionAttribute]);
        geometry.callMethod("addAttribute",["customColor",colorAttribute]);
        geometry.callMethod("addAttribute",["size",sizeAttribute]);


    }

    void addForce(Vector3 vec){

    }

    void updateParticles(){
        for(var i = 0;i<this.num_particles;++i){
            sizes[i] = 20.0;
            sizes[i * 3] = (random.nextDouble() * (2-1)) * radius;
            sizes[i * 3 + 1] = (random.nextDouble() * (2-1)) * radius;
            sizes[i * 3 + 2] = (random.nextDouble() * (2-1)) * radius;

            color.callMethod("setHSL",[(i / num_particles),1.0,0.5]);

            colors[i * 3] = color["r"];
            colors[i * 3 + 1] = color["g"];
            colors[i * 3 + 2] = color["b"];
        }
    }

    void addMaterial(Material m){
        material = m;
    }

    void addTo(JsObject scene){

        pointCloud = Three.make("PointCloud",[geometry,material.getObject()]);
        scene.callMethod("add",[pointCloud]);
    }
}
