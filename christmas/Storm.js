var SnowFlake = require("./SnowFlake");


var Storm = function(num){
    num = num !== undefined ? num : 100;

    var particles = [];
    for(var i = 0;i<num;++i){
        particles.push(new SnowFlake());
    }

    this.particles = particles;
};
