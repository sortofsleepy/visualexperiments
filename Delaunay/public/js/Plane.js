var Plane = function(width,height,segments,slices){
    this.triangles = [];
    this.width = width || 100;
    this.height = height || 100;
    this.segments = segments || 4;
    this.slices = slices || 4;
    this.segmentWidth = this.width / this.segments;
    this.sliceHeight = this.height / this.slices;
    this.vertices = [];

    // Cache Variables
    var x, y, v0, v1, v2, v3,
        vertex, triangle, vertices = [],
        offsetX = this.width * -0.5,
        offsetY = this.height * 0.5;

    // Add Vertices
    for (x = 0; x <= this.segments; x++) {
        vertices.push([]);
        for (y = 0; y <= this.slices; y++) {
            vertex = new THREE.Vector3(offsetX + x*this.segmentWidth - 40, offsetY - y*this.sliceHeight,0);

            vertices[x].push(vertex);
            this.vertices.push(vertex);
        }
    }

    // Add Triangles
    for (x = 0; x < this.segments; x++) {
        for (y = 0; y < this.slices; y++) {
            v0 = vertices[x+0][y+0];
            v1 = vertices[x+0][y+1];
            v2 = vertices[x+1][y+0];
            v3 = vertices[x+1][y+1];

            t0 = new BufferTriangle([v0, v1, v2]);
            t1 = new BufferTriangle([v2, v1, v3]);
            this.triangles.push(t0, t1);
        }
    }
};

Plane.prototype = {
    render:function(){
        var len = this.triangles.length;
        for(var i = 0;i<len;++i){
            var triangle = this.triangles[i];

        }
    },

    /**
     * Sets a new vector for the triangle
     * @param vec new THREE.js vector
     */
    setPosition:function(vec){
        var len = this.triangles.length;
        for(var i = 0;i<len;++i){
            var triangle = this.triangles[i];
            triangle.position.set(vec.x,vec.y,vec.z);
        }
    },

    jiggle:function(){
        var len = this.triangles.length;
        for(var i = 0;i<len;++i){
            var triangle = this.triangles[i];
            triangle.position.set(vec.x,vec.y,vec.z);
        }
    }

}; // end plane prototype