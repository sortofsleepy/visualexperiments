var sys = require('pex-sys');
var glu = require('pex-glu');
var materials = require('pex-materials');
var color = require('pex-color');
var gen = require('pex-gen');
var geom = require("pex-geom");
var gui = require("pex-gui");

var ObjLoader = require("./ObjLoader");

var GUI = gui.GUI;
var Geometry = geom.Geometry;
var Cube = gen.Cube;
var Mesh = glu.Mesh;
var ShowColors= materials.ShowColors;
var ShowNormals = materials.ShowNormals;
var PerspectiveCamera = glu.PerspectiveCamera;
var Arcball = glu.Arcball;
var Color = color.Color;
var Platform = sys.Platform;
var Vec3 = geom.Vec3;


sys.Window.create({
  settings: {
    width: 1280,
    height: 720,
    type: '3d',
    fullscreen: Platform.isBrowser ? true : false
  },
  init: function() {




      this.camera = new PerspectiveCamera(75, this.width / this.height,1,100000);
      this.arcball = new Arcball(this, this.camera);


  },
  draw: function() {
      glu.clearColorAndDepth(Color.Black);
      glu.enableDepthReadAndWrite(true);

  }
});
