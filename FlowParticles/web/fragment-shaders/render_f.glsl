precision mediump float;
precision mediump int;

uniform float time;


varying vec4 vColor;

void main()	{

	vec4 color = vec4( vColor );
	color.r = 1.0;

	gl_FragColor = vec4(color);

}
