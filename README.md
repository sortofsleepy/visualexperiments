# Visual experimentation #

Just some random things here and there that I've been/meant to continue exploring. Some work, some don't, and some are just not complete. 
Some of these projects I have not looked at in some time so I might get the technical details wrong here and there.

So far

[Delaunay](https://bitbucket.org/sortofsleepy/visualexperiments/src/9b024e7d37af814cad0f75be33e9f6ef08026174/Delaunay/?at=master)
A experiment in Delaunay triangulation using Three.js. Should create a Delaunay triangulated mesh with random colors. Theres a Express server
included in the repo but you could just as easily run 
```
#!python

python -m SimpleHTTPServer
```
from within the public folder too.

---
 [Fields](https://bitbucket.org/sortofsleepy/visualexperiments/src/9b024e7d37af814cad0f75be33e9f6ef08026174/Fields/?at=master)
Just a simple experiment to create a oscillating grid of particles. This was done using the "glNext" branch of the [Cinder framework](http://www.libcinder.org).

---
[FlowParticles](https://bitbucket.org/sortofsleepy/visualexperiments/src/a3ac5c4d0c25d0b0cf2df1ad6977943654ba2394/FlowParticles/?at=master)
at the time I was exploring a basic flow field algorithm and decided to try using [Dart](https://www.dartlang.org/). I started working with Dart and integrated Three.js
with the project. At the moment it remains unfinished. 

---
[Waves](https://bitbucket.org/sortofsleepy/visualexperiments/src/a3ac5c4d0c25d0b0cf2df1ad6977943654ba2394/Waves/?at=master)
Basically the same thing as  [Fields](https://bitbucket.org/sortofsleepy/visualexperiments/src/9b024e7d37af814cad0f75be33e9f6ef08026174/Fields/?at=master), but
done instead with Three.js. A little farther along though :)

---
[Waves2](https://bitbucket.org/sortofsleepy/visualexperiments/src/a3ac5c4d0c25d0b0cf2df1ad6977943654ba2394/Waves2/?at=master)
Same thing as waves but with a little more visual flare
---
[Birds](https://bitbucket.org/sortofsleepy/visualexperiments/src/a3ac5c4d0c25d0b0cf2df1ad6977943654ba2394/birds/?at=master) 
A simple flocking simulation done with a pseudo-bird model that actually flaps it wings as it flies along. 
Done in Three.js

---
[Christmas](https://bitbucket.org/sortofsleepy/visualexperiments/src/a3ac5c4d0c25d0b0cf2df1ad6977943654ba2394/christmas/?at=master)
Theres not all that much at the moment. I was exploring and learning about Marcin Ignac's [Pex framework](https://github.com/vorg/pex)
which is a webgl framework based on [Plask](http://www.plask.org/)(the difference is primarily that Ignac's is meant to be used for the web while
Plask remains desktop focused). At the moment all it does is load a snowflake model, but I ended up having to (hackily) adapt Three.js's ObjLoader to get it to work
---
[particle_connections](https://bitbucket.org/sortofsleepy/visualexperiments/src/a3ac5c4d0c25d0b0cf2df1ad6977943654ba2394/particle_connections/?at=master)
Nothing too exciting here, a bunch or particles just launch and if they get close enough to a neighbor, a connection forms. This was done with [PIxi.js](http://www.pixijs.com/)