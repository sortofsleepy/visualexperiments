import Pixi;
import js.Browser.*;
import js.html.*;
import math.Vector;
import ParticleSystem;
class Background{
    
    var renderer:Dynamic;
    var stage:Dynamic;
    
    var mouseX:Float;
    var mouseY:Float;
    
    var system:ParticleSystem;
    
    var backgroundColor = "";
    var particleColor = "";

    public function new(){
        renderer = Pixi.Renderer(window.innerWidth,window.innerHeight);
        renderer.view.style.background = "#ffffff";
        
        //grab color from url
        var segments = window.location.href.split("?")[1];
        //replace the ending slash
        segments = segments.split("/")[0];
        
        //split the params by the & symbol
        var params = segments.split("&");

        for(i in 0...params.length){
            var param = params[i].split("=");
            
            if(param[0] == "bgcolor"){
                backgroundColor = param[1];   
            }
                
            if(param[0] == "particlecolor"){
                particleColor = param[1];   
            }
         
           
        }
        
        stage = Pixi.Stage(backgroundColor);

        document.querySelector("body").appendChild(renderer.view);
        
        system = new ParticleSystem(200,stage,particleColor);

        system.addForce();
        
        
        
        events();
        run();
    }
    
    public function events(){
                //if the viewport gets resized, adjust camera and renderer
        var resize : Event -> Void = function(e:Dynamic){
            renderer.view.width = window.innerWidth;
            renderer.view.height = window.innerHeight; 
        }
            
            
        var mousemove : Event -> Void = function(e:Dynamic){
            mouseX = ( e.clientX - window.innerWidth / 2 ) * 0.5;
            mouseY = ( e.clientY - window.innerHeight / 2 ) * 0.5;   
        }
     
     
        window.addEventListener("mousemove",mousemove);
        window.addEventListener("resize",resize);
           
    }
    
      public function run(){
        //run request animation frame
        this.requestAnimationFrame(run);
        system.draw();
        renderer.render(stage);
    }
    

    public function requestAnimationFrame(method:Dynamic){
        var requestAnimationFrame:Dynamic = untyped window.requestAnimationFrame;
        if(requestAnimationFrame == null){
            requestAnimationFrame = function(method:Dynamic):Void{
                untyped __js__("window.setTimeout")(method,1000/60);   
            }
        }
        untyped __js__("requestAnimationFrame")(method);
    }
    
}