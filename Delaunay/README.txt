Delaunay 

Generates a Delaunay triangulated mesh using Three.js. Sadly the implementation I found that this is based off of, though being reasonably understandable, was also 
very very slow and inefficient so trying to animate this particular mesh is difficult.

I don't recall why, but it's packaged as a node.js express app, so you'll have to run npm install first.
Afterwards run 

node app.js

and you should be able to see something at localhost:3000 in your browser.