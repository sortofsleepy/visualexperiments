package math;

import Math;

/**
 *  A simple vector class based/taken off a bunch of different libs out there(but primarily glMatrix)
 *  TODO integrate TypedArrays for Js?
 */

class Vector{
    public var x:Float;
    public var y:Float;
    public var z:Float;
    

    public function new(?x:Float=0,?y:Float=0,?z:Float=0){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /**
     *  Clones this Vec3 and returns a copy;
     */
    public function clone(){
        return new Vector(this.x,this.y,this.z);
    }
    
    public function copy(vec:Vector){
        this.x = vec.x;
        this.y = vec.y;
        this.z = vec.z;
        
        return this;
    }
    
    public function add(vec:Vector){
        this.x += vec.x;
        this.y += vec.y;
        this.z += vec.z;
    }
    
    public function subtract(vec:Vector){
        this.x -= vec.x;
        this.y -= vec.y;
        this.z -= vec.z;
        
        return this;
    }
    
    static public function sub(v1:Vector,v2:Vector){
        var vec = new Vector(v1.x - v2.x,v1.y - v2.y, v1.z - v2.z);
        return vec;
    }
    
    //multiplies the vecotr
    public function multiply(vec:Vector){
        this.x *= vec.x;
        this.y *= vec.y;
        this.z *= vec.z;
        
        return this;
    }
    
    public function multiplyScalar(val:Float){
         this.x *= val;
        this.y *= val;
        this.z *= val;
    }
    
    
    //divide the vector by another vector
    public function divide(vec:Vector){
        this.x /= vec.x;
        this.y /= vec.y;
        this.z /= vec.z;
        
        return this;
    }
    
    //divide all the values of a vector by a singular value
    public function divideScalar(val:Float){
        this.x /= val;
        this.y /= val;
        this.z /= val;
    }
    
    //resets values to 0
    public function reset(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public function distance(vec:Vector){
        var x = vec.x - this.x,
        y = vec.y - this.y,
        z = vec.z - this.z;
        return Math.sqrt(x*x + y*y + z*z);
    }
    
    static public function dist(vec1:Vector,vec2:Vector){
        var x = vec2.x - vec1.x;
        var y = vec2.y - vec1.y;
        var z = vec2.z - vec1.z;
        return Math.sqrt(x*x + y*y + z*z);
    }
    
    public function distanceSquared(vec:Vector){
         var x = vec.x - this.x,
        y = vec.y - this.y,
        z = vec.z - this.z;
        return x*x + y*y + z*z;
    }
    
    /**
     *  Normalizes a vector
     */
    public function normalize(){
        var x = this.x,
        y = this.y,
        z = this.z;
        var len = x*x + y*y + z*z;
        if (len > 0) {
            //TODO: evaluate use of glm_invsqrt here?
            len = 1 / Math.sqrt(len);
            this.x *= len;
            this.y *= len;
            this.z *= len;
        }
        return this;  
    }
    
    
   /**
    * Performs a linear interpolation between two vec3's
    *
    * @param {vec3} vec the receiving vector
    * @param {vec3} a the first operand
    * @param {vec3} b the second operand
    * @param {Number} t interpolation amount between the two inputs
    * @returns {vec3} out
    */
    public function lerp(vec:Vector,t:Dynamic){
        this.x = this.x + t * (vec.x - this.x);
        this.y = this.y + t * (vec.y - this.y);
        this.z = this.z + t * (vec.z - this.z);
        
        return this;       
    }
    
    public function rotateX(vec:Vector,rot:Float){
        var p = [], r=[];
        
        //Translate point to the origin
        p[0] = this.x - vec.x;
        p[1] = this.y - vec.y;
        p[2] = this.z - vec.z;
        
        //perform rotation
        r[0] = p[0];
        r[1] = p[1]*Math.cos(rot) - p[2]*Math.sin(rot);
        r[2] = p[1]*Math.sin(rot) + p[2]*Math.cos(rot);
        
        this.x = r[0];
        this.y = r[1];
        this.z = r[2];
        
        return this;
    }
    
    public function rotateY(vec:Vector,rot:Float){
         var p = [], r=[];
        
        //Translate point to the origin
        p[0] = this.x - vec.x;
        p[1] = this.y - vec.y;
        p[2] = this.z - vec.z;
        
        //perform rotation
        r[0] = p[2]*Math.sin(rot) + p[0]*Math.cos(rot);
        r[1] = p[1];
        r[2] = p[2]*Math.cos(rot) - p[0]*Math.sin(rot);
        
        this.x = r[0];
        this.y = r[1];
        this.z = r[2];
        
        return this;
    }
    
    public function rotateZ(vec:Vector,rot:Float){
        var p = [], r=[];
        
        //Translate point to the origin
        p[0] = this.x - vec.x;
        p[1] = this.y - vec.y;
        p[2] = this.z - vec.z;
        
        //perform rotation
        r[0] = p[0]*Math.cos(rot) - p[1]*Math.sin(rot);
        r[1] = p[0]*Math.sin(rot) + p[1]*Math.cos(rot);
        r[2] = p[2];
        
        this.x = r[0];
        this.y = r[1];
        this.z = r[2];
        
        return this;
    }
    
    public function limit(max:Float){
        if(magSq() > max * max){
            normalize();
            multiplyScalar(max);
        }
    }
    
    public function mag(){
        return Math.sqrt(x*x + y * y + z * z);   
    }
    
    public function magSq(){
        return (x * x + y * y + z * z);   
    }
    
    public function str(){
        return 'vec3(' + this.x + ', ' + this.y + ', ' + this.z + ')';
    }
}

/**========= OPERATOR OVERLOADS ============*/

@:forward(add,subtract,multiply,divide)
abstract Vec3(Vector){
    public function new() {
        this = new Vector();
    }   
}