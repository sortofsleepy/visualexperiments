package base;

import math.Vector;

class DisplayObject{
    
    //general properties
    public var acceleration:Vector;
    public var velocity:Vector;
    public var position:Vector;
    
    //mass of the object
    var mass:Float = 10.0;
    
    var force = {};
    
    var torque = 0;
    var speed = 0;
    var angularSpeed = 0;
    var angularVelocity = 0;
    var isStatic = false;
    var isSleeping = false;
    var motion = 0;
    var sleepThreshold = 60;
    var density = 0.001;
    var restitution = 0;
    var friction = 0.1;
    var frictionAir = 0.01;
    
    var boundries = {
        x:0,
        y:0
    }
    
    public function new(){
        acceleration = new Vector();
        velocity = new Vector();
        position = new Vector();
    }
    
    public function addForce(force:Vector){
        //clone so we don't alter original values
        var copy:Vector = force.clone();
        
        //divide by the mass
        copy.divideScalar(mass);
        
        //add to accelleration
        acceleration.add(copy);
        
  
    }
    
    public function update(){
       
        velocity.add(acceleration);
        position.add(velocity);
        checkBoundries();
        
        //reset acceleration? seems to cause unexpected things normally
        acceleration.reset();
    }
    
    public function setBoundries(xBounds:Int,yBounds:Int){
        boundries.x = xBounds;
        boundries.y = yBounds;
    }
    
  
    
    //TODO adjust for object width, but not important for now.
    public function checkBoundries(){
        if(position.x > boundries.x){
            position.x = boundries.x;
            velocity.x *= -1;
        }else if(position.x < 0){
            velocity.x *= -1;
            position.x = 0;
        }
            
        if(position.y > boundries.y){
            position.y = boundries.y;
            velocity.y *= -1;
        }else if(position.y < 0){
            velocity.y *= -1;
            position.y = 0;
        }
            
            
    }
    
    public function getVelocity(){
        return velocity;   
    }
    
    //checks the distance between this object and another one
    public function distanceCheck(target:DisplayObject){
        
    }
}
