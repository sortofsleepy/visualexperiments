var count = 0;
var BufferTriangle = function(point1,point2,point3){
    this.a = point1 || new THREE.Vector3();
    this.b = point2 || new THREE.Vector3();
    this.c = point3 || new THREE.Vector3();

    this.verts = [this.a,this.b,this.c];

    this.u = new THREE.Vector3();
    this.v = new THREE.Vector3();
    this.centroid = new THREE.Vector3();
    this.normal = new THREE.Vector3();
    this.color = new THREE.Color();
    this.time = 0;
    this.shaderData = {

        uniforms: {
            time: { type: "f", value: this.time }
        },
        vertexShader: document.getElementById( 'vertexShader' ).textContent,
        fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
        side: THREE.DoubleSide,
        transparent: true

    }
    var geometry = new THREE.BufferGeometry();
    this.geometry = geometry;
    this.angle = 0;


};

BufferTriangle.prototype = {
    computeCentroid:function(){
        this.centroid.x = this.a.x + this.b.x + this.c.x;
        this.centroid.y = this.a.y + this.b.y + this.c.y;
        this.centroid.z = this.a.z + this.b.z + this.c.z;
        this.centroid.divideScalar(3);
        return this;
    },

    inCircumcircle:function(v){
        this.circumcircle();
        var dx = this.center.x - v.x;
        var dy = this.center.y - v.y;


        var dist_squared = dx * dx + dy * dy;
        return (dist_squared <= this.radius_squared);
    },

    circumcircle:function(){

        var EPSILON = 1.0e-6;

        this.v0 = this.a;
        this.v1 = this.b;
        this.v2 = this.c;
        // From: http://www.exaflop.org/docs/cgafaq/cga1.html

        var A = this.v1.x - this.v0.x;
        var B = this.v1.y - this.v0.y;
        var C = this.v2.x - this.v0.x;
        var D = this.v2.y - this.v0.y;

        var E = A * (this.v0.x + this.v1.x) + B * (this.v0.y + this.v1.y);
        var F = C * (this.v0.x + this.v2.x) + D * (this.v0.y + this.v2.y);

        var G = 2.0 * (A * (this.v2.y - this.v1.y) - B * (this.v2.x - this.v1.x));

        var dx, dy;

        if (Math.abs(G) < EPSILON) {
            // Collinear - find extremes and use the midpoint

            var minx = Math.min(this.v0.x, this.v1.x, this.v2.x);
            var miny = Math.min(this.v0.y, this.v1.y, this.v2.y);
            var maxx = Math.max(this.v0.x, this.v1.x, this.v2.x);
            var maxy = Math.max(this.v0.y, this.v1.y, this.v2.y);

            this.center = new THREE.Vector3((minx + maxx) / 2, (miny + maxy) / 2,0);

            dx = this.center.x - minx;
            dy = this.center.y - miny;
        } else {
            var cx = (D * E - B * F) / G;
            var cy = (A * F - C * E) / G;

            this.center = new THREE.Vector3(cx, cy,0);

            dx = this.center.x - this.v0.x;
            dy = this.center.y - this.v0.y;
        }

        this.radius_squared = dx * dx + dy * dy;
        this.radius = Math.sqrt(this.radius_squared);

    },

    buildGeometry:function(){
        var geometry = this.geometry;
        var color = new THREE.Color();
        var colors = new Float32Array( 3 * 3 );

        var n = 1000, n2 = n / 2; // particles spread in the cube

        /** =============== SET POSITIONS ================== */

        var buffer = new Float32Array(9);
        for(var i = 0;i<3;++i){
            buffer[i * 3] = this.verts[i].x;
            buffer[i * 3 + 1] = this.verts[i].y;
            buffer[i * 3 + 2] = this.verts[i].z;
        }
        var position = new THREE.BufferAttribute(buffer,3);
        geometry.addAttribute("position",position);
        /*var position = new THREE.BufferAttribute(new Float32Array(3 * 3),3);
         var positions = [
         this.a.x,this.a.y,this.a.z,
         this.b.x,this.b.y,this.b.z,
         this.c.x,this.c.y,this.c.z
         ];


         position.array.set(positions);
         geometry.addAttribute("position",position);*/
        /** =============== SET COLORS ================== */
        for(var i = 0;i<9;++i){
            var x = Math.random() * n - n2;
            var y = Math.random() * n - n2;
            var z = Math.random() * n - n2;

            // colors

            var vx = ( x / n ) + 0.5;
            var vy = ( y / n ) + 0.5;
            var vz = ( z / n ) + 0.5;

            color.setRGB( vx, vy, vz );

            colors[ i ]     = color.r;
            colors[ i + 1 ] = color.g;
            colors[ i + 2 ] = color.b;
        }
        geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );
        geometry.dynamic = true;

        var material = new THREE.RawShaderMaterial( this.shaderData );


        var mesh = new THREE.Mesh( geometry, material );
        this.mesh = mesh;
        scene.add(this.mesh);
        var position = this.geometry.attributes.position.array;

    },

    update:function(){

        var color = this.geometry.attributes.color.array;
        var position = this.geometry.attributes.position.array;
        var time = Date.now() * 0.005;
        this.angle += 0.05;

       // this.mesh.rotation.x += Math.random()*0.05;
        //this.mesh.rotation.y += 0.05 * Math.sin(this.angle) * Math.random();
        for( var i = 0; i <9; i++ ) {



            color [i] =  1 * ( 1 + Math.sin( 0.1 * i + time ) );

        }



        this.geometry.attributes.position.needsUpdate = true;
        this.geometry.attributes.color.needsUpdate = true;
    }

}; // end prototype


/**
 * Creates a new edge
 * @param vec a THREE.Vector 3 position for the edge
 * @constructor
 */
var BufferEdge = function(vec,vec2,vec3){
    this.geometry = new THREE.BufferGeometry();
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z

    this.material = new THREE.LineBasicMaterial({
        vertexColors:THREE.VertexColors
    });
};

BufferEdge.prototype = {
    /**
     * Sees if two vectors match
     * @param other another THREE.Vector3
     * @returns {boolean}
     */
    equals:function(other){
        return (this.x === other.x && this.y === other.y && this.z === other.z);
    },

    /**
     * BUilds the geometry to create the line
     * @private
     */
    _buildGeometry:function(){
        var position = new THREE.BufferAttribute(new Float32Array(3),3);
        var positions = [
            this.x,this.y,this.z
        ];

        position.array.set(positions);
        this.geometry.addAttribute("position",position);
        this.mesh = new THREE.Line(this.geometry,this.material);

    }
}


/*  var material = new THREE.RawShaderMaterial( {

 uniforms: {
 time: { type: "f", value: 1.0 }
 },
 vertexShader: document.getElementById( 'vertexShader' ).textContent,
 fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
 side: THREE.DoubleSide,
 transparent: true

 } );


 var mesh = new THREE.Mesh( geometry, material );
 this.mesh = mesh;
 scene.add(this.mesh)*/