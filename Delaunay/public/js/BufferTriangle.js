var MESH = {
    width: 1.2,
    height: 1.2,
    depth: 10,
    segments: 16,
    slices: 8,
    xRange: 0.8,
    yRange: 0.1,
    zRange: 1.0,
    ambient: '#555555',
    diffuse: '#FFFFFF',
    speed: 0.002
};

var ox, oy, oz, l, light, v, vertex, offset = MESH.depth/2;

/**
 * A triangle shape using Buffer geometry
 * @param vertices{Array} array of the three points on a triangle
 * @param options{Object} object of other possible properties for the triangle
 * @constructor
 */
var BufferTriangle = function(vertices,options){
    options = options || {}
    //vertices of the triangle
    this.vec1 = vertices[0];
    this.vec2 = vertices[1];
    this.vec3 = vertices[2];

    this.verts = [this.vec1,this.vec2,this.vec3];

    //geometry
    this.geometry = new THREE.BufferGeometry();

    //texture coordinates
    this.u = options.u || new THREE.Vector3();
    this.v = options.v || new THREE.Vector3();
    this.centroid = new THREE.Vector3();
    this.normal = new THREE.Vector3();
    this.color = new THREE.Color();
    this.time = 0;

    var shader = options.shaders || {}
    var vertex = shader.vertex || document.getElementById( 'vertexShader' ).textContent;
    var fragment =shader.fragment || document.getElementById( 'fragmentShader' ).textContent;

    this.step = 0.5;
    //data to pass to shader
    this.shaderData = {

        uniforms: {
            time: { type: "f", value: this.time }
        },
        vertexShader: vertex,
        fragmentShader: fragment,
        side: THREE.DoubleSide,
        transparent: true
    }



};

BufferTriangle.prototype = {


    /**======= BUILDING =============*/
    buildGeometry:function(){
        var geometry = this.geometry;
        var color = this.color;
        /**
         * Build out all the geometry for the position of the vertices.
         * 9 vertices for 3 points
         */
        //set new Typed array to hold values
        var buffer = new Float32Array(9);

        //set the values
        for(var i = 0;i<3;++i){
            buffer[i * 3] = this.verts[i].x;
            buffer[i * 3 + 1] = this.verts[i].y;
            buffer[i * 3 + 2] = this.verts[i].z;
        }

        //input into BufferAttribute
        var position = new THREE.BufferAttribute(buffer,3);

        //add to geometry : Note "position" attribute needs to be specified in shader
        geometry.addAttribute("position",position);
        var n = 1000, n2 = n / 2; // particles spread in the cube

        /** =============== SET COLORS ================== */
        var colors = new Float32Array( 3 * 3 );

        for(var i = 0;i<9;++i){
            var x = Math.random() * n - n2;
            var y = Math.random() * n - n2;
            var z = Math.random() * n - n2;

            // colors

            var vx = ( x / n ) + 0.5;
            var vy = ( y / n ) + 0.5;
            var vz = ( z / n ) + 0.5;

            color.setRGB( vx, vy, vz );

            colors[ i ]     = color.r;
            colors[ i + 1 ] = color.g;
            colors[ i + 2 ] = color.b;
        }
        geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );
        geometry.dynamic = true;

        /** Compile to mesh */
        var material = new THREE.RawShaderMaterial( this.shaderData );


        var mesh = new THREE.Mesh( geometry, material );
        this.mesh = mesh;



        //add to scene
        //TODO should this be global? Pass in scene instead?
        scene.add(this.mesh);



    },

    update:function(){
        var color = this.geometry.attributes.color.array;
        var position = this.geometry.attributes.position.array;
        var time = Date.now() * 0.005;
        this.angle += 0.05;

        // this.mesh.rotation.x += Math.random()*0.05;
        //this.mesh.rotation.y += 0.05 * Math.sin(this.angle) * Math.random();
        for( var i = 0; i <9; i++ ) {



        }

        this.geometry.attributes.position.needsUpdate = true;
        this.geometry.attributes.color.needsUpdate = true;
    },

    /**======= MATH =============*/
    /**
     * Tests to see whether or not a Vector is in bounds
     * of another vector
     * @param v{THREE.Vector3} the other vector
     * @returns {boolean}
     */
    inCircumcircle:function(v){
        this.circumcircle();
        var dx = this.center.x - v.x;
        var dy = this.center.y - v.y;


        var dist_squared = dx * dx + dy * dy;
        return (dist_squared <= this.radius_squared);
    },

    /**
     * Calculates the "radius" of this Triangle's vector
     */
    circumcircle:function(){

        var EPSILON = 1.0e-6;

        this.v0 = this.vec1;
        this.v1 = this.vec2;
        this.v2 = this.vec3;
        // From: http://www.exaflop.org/docs/cgafaq/cga1.html

        var A = this.v1.x - this.v0.x;
        var B = this.v1.y - this.v0.y;
        var C = this.v2.x - this.v0.x;
        var D = this.v2.y - this.v0.y;

        var E = A * (this.v0.x + this.v1.x) + B * (this.v0.y + this.v1.y);
        var F = C * (this.v0.x + this.v2.x) + D * (this.v0.y + this.v2.y);

        var G = 2.0 * (A * (this.v2.y - this.v1.y) - B * (this.v2.x - this.v1.x));

        var dx, dy;

        if (Math.abs(G) < EPSILON) {
            // Collinear - find extremes and use the midpoint

            var minx = Math.min(this.v0.x, this.v1.x, this.v2.x);
            var miny = Math.min(this.v0.y, this.v1.y, this.v2.y);
            var maxx = Math.max(this.v0.x, this.v1.x, this.v2.x);
            var maxy = Math.max(this.v0.y, this.v1.y, this.v2.y);

            this.center = new THREE.Vector3((minx + maxx) / 2, (miny + maxy) / 2,0);

            dx = this.center.x - minx;
            dy = this.center.y - miny;
        } else {
            var cx = (D * E - B * F) / G;
            var cy = (A * F - C * E) / G;

            this.center = new THREE.Vector3(cx, cy,0);

            dx = this.center.x - this.v0.x;
            dy = this.center.y - this.v0.y;
        }

        this.radius_squared = dx * dx + dy * dy;
        this.radius = Math.sqrt(this.radius_squared);

    }
}