import Particle;
import js.Browser.*;
import math.Vector;
class ParticleSystem{
    var children = [];
   
    public function new(num:Int,stage:Dynamic,?color:Dynamic){
        for(i in 0...num){
            var particle = new Particle(10,10,4,color);
            particle.addTo(stage);
            children.push(particle);    
        }
    
        for(i in 0...children.length){
            var child = children[i];
            child.setNeighbors(children);
           
        }
    }

    public function setColor(color:Dynamic){
        for(i in 0...children.length){
            children[i].setColor(color);
        }   
    }
    public function addForce(){
        for(i in 0...children.length){
            children[i].addForce(new Vector(Math.cos(Std.random(99)),Std.random(99),0));
        }
    }
        
        
        
    public function draw(){
        for(i in 0...children.length){
            children[i].flock();
            children[i].draw();
        }
    }
}