import base.DisplayObject;
import js.Browser.*;
import js.html.*;
import math.Vector;
    
class Particle extends DisplayObject{
    var graphics:Dynamic;
    var lineGraphics:Dynamic;
    
    var radius:Int;
    public var neighbors:Array<Particle>;
    
    var maxspeed:Float = 0.5;
    var maxforce:Float = 2;
    
    public var stage:Dynamic;
    
    public var particleColor:Dynamic;
    
    public function new(?x:Float=10,?y:Float=10,?radius:Int=4,?color:Dynamic){
        super();
        graphics = untyped __js__("new PIXI.Graphics");
        lineGraphics = untyped __js__("new PIXI.Graphics");
            
        if(color){
            particleColor = color;   
        }else{
            particleColor = 0x00FF00;
        }
        this.radius = radius;
        graphics.beginFill(particleColor);
        
        lineGraphics.lineStyle(2,0x83B291,1);
        graphics.drawCircle(x,y,radius);
        
        //set the boundries for the object
        setBoundries(window.innerWidth,window.innerHeight);
        
        //set initial velocity
        position.x = Std.random(window.innerWidth);
        position.y = Std.random(window.innerHeight);
    
        //if the viewport gets resized, adjust camera and renderer
        var resize : Event -> Void = function(e:Dynamic){
            setBoundries(window.innerWidth,window.innerHeight);  
        }
              
        window.addEventListener("resize",resize);
    }
    
    public function setColor(color:Dynamic){
        graphics.beginFill(color);
    }
    
    public function setNeighbors(neighbors:Dynamic){
        this.neighbors = neighbors;   
    }
    
    public function flock(){
       var sep = seperate();
        
        sep.multiplyScalar(1.5);
        
        addForce(sep);
    }
    
    public function addTo(stage:Dynamic){
        stage.addChild(graphics);   
        this.stage = stage;
    }
    
    public function draw(){
        update();
       
        graphics.position.x = this.position.x;
        graphics.position.y = this.position.y;  
    }
    
    
    public function seperate(){
        var desiredseperation = 25.0;
        var steer = new Vector();
        var count = 0;
              stage.addChild(lineGraphics);
        for(i in 0...neighbors.length){
            var other = neighbors[i];
            var d = Vector.dist(position,other.position);
            
         
            if(d < 50){
                lineGraphics.clear();
                lineGraphics.lineStyle(2,0x00FF00,1);
                lineGraphics.moveTo(position.x + radius + 4, position.y+radius + 5);
                lineGraphics.lineTo(other.position.x + radius + 4,other.position.y + radius + 5);
            }else if(d > 1200){
                 lineGraphics.clear();
            }
         
            if((d > 0) && (d < desiredseperation)){
           
             
                
                var diff = Vector.sub(position,other.position);
                diff.normalize();
                diff.divideScalar(d);
                steer.add(diff);
                count++;
            }
        }
        
        if(count > 0){
            steer.divideScalar(count);   
        }
            
        if(steer.mag() > 0){
            steer.normalize();
            steer.multiplyScalar(maxspeed);
            steer.subtract(velocity);
            steer.limit(maxforce);
        }
            
        return steer;    
    
    }
    
}
