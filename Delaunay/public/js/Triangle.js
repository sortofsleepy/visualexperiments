

var Triangle = function(vec1,vec2,vec3){
    this.x = new Two.Anchor(vec1[0],vec1[1]);
    this.y = new Two.Anchor(vec2[0],vec2[1]);
    this.z = new Two.Anchor(vec3[0],vec3[1]);

    this.shape = new Two.Polygon([this.x,this.y,this.z],true);
};