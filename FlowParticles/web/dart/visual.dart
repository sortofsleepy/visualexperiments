import 'dart:html';
import 'dart:js';
import 'dart:math';
import 'THREE/THREE.dart';
import 'package:vector_math/vector_math.dart';
import 'dart:typed_data';
import 'System.dart';

var stats = Three.Stats();
var renderer = new Renderer();
var renderPass = new ShaderPass();
var scene = Three.Scene();
var camera = Three.PerspectiveCamera(40.0,window.innerWidth/window.innerHeight,1.0,10000.0);

//add the flow field.
var field = new FlowField();

//add a random number generator
var rand = new Random();
var shaderMat;
var mesh;

void main() {
    camera["position"]["z"] = 300;

    //load shaders for the pass
    renderPass.loadShader("./vertex-shaders/particle_vertex.glsl","./fragment-shaders/bloom-fs.glsl");

    //send to renderer
    renderer.addPass(renderPass);

    //var ambient = new THREE.AmbientLight( 0x444444 );
    //scene.add( ambient );

    var light = new JsObject(context["THREE"]["AmbientLight"],[0xffffff]);
    scene.callMethod("add",[light]);

    var attributes = {
        'size':        { 'type': 'f', 'value': 'null' },
        'customColor': { 'type': 'c', 'value': 'null' }
    };



    var test = new JsObject.jsify({
        "uniforms": {
            "time": { "type": "f", "value" :1.0 },
            "desired":{"type":"v3","value":null}
        },

        "vertexShader":Loader.loadShader("./vertex-shaders/render_v.glsl"),
        "fragmentShader":Loader.loadShader("./fragment-shaders/render_f.glsl"),
        "side":context["THREE"]["DoubleSide"]
    });

    shaderMat = new JsObject(context["THREE"]["ShaderMaterial"],[test]);



    var geo = new JsObject(context["THREE"]["PlaneGeometry"],[10,10,2,2]);
    var mat = new JsObject(context["THREE"]["MeshBasicMaterial"],[{
        "color":0xffffff
    }]);
    mesh = new JsObject(context["THREE"]["Mesh"],[geo,shaderMat]);
    scene.callMethod("add",[mesh]);


    setupRenderer();

    draw();
}

void setupRenderer(){
    //make the renderer the same size as the window
    renderer.setSize(window.innerWidth,window.innerHeight);
    querySelector("body").append(renderer.get()["domElement"]);
    querySelector("body").append(stats["domElement"]);


    //resize canvas when window is resized
    window.onResize.listen((e){
        camera["aspect"] = window.innerWidth / window.innerHeight;
        camera.callMethod("updateProjectionMatrix");
        renderer.setSize(window.innerWidth,window.innerHeight);
    });

}


void draw(){
    window.requestAnimationFrame(run);
}


void run(num time){
    window.requestAnimationFrame(run);

    shaderMat["uniforms"]["time"]["value"] = time * 0.004;
    Vec3 desired = field.getLocation(mesh["position"]);

    shaderMat["uniforms"]["desired"]["value"] = desired.get();


    renderer.render(scene,camera);
    stats.callMethod("update");
}

/* Vec3 desired = field.getLocation(mesh["position"]);

    shaderMat["uniforms"]["time"]["value"] = time * 0.04;
    shaderMat["uniforms"]["desired"]["value"] = desired.get();


    mesh["rotation"]["x"] += 0.05;
    mesh["rotation"]["y"] += 0.05;
*/
