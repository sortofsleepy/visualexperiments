var geom = require("pex-geom");
var color = require("pex-color");

var Color = color.Color;
var Vec3 = geom.Vec3;
var Vec2 = geom.Vec2;
var Vec4 = geom.Vec4;
var Face3 = require("./Face3");
var Geometry = geom.Geometry;

/**
 * Generalized model loader that can load JSON model data
 * formatted for THREE.
 * Adapted from THREE.JSONLoader - some functionality possibly missing.
 * @type {{loadModel: Function}}
 */

var ObjLoader = {

    loadModel:function(path){
        var req = new XMLHttpRequest();
        req.open("GET",path,false);
        req.send();

        if(req.readyState === 4){
            if(req.status === 200){
                var data = JSON.parse(req.responseText);
               return this._parseModel(data);
            }
        }

    },

    /**
     * Stripes away all the other meta data on a Face3 and returns the
     * props in a straight array
     * @param facesArray
     */
    stripFaces:function(facesArray){
        var length = facesArray;
        var stripped = [];
        for(var i = 0;i<length;++i){
            var face = facesArray[i];
            stripped.push(face.a);
            stripped.push(face.b);
            stripped.push(face.c);
        }

        return stripped;
    },

    _parseModel:function(json){
        var scope = this,
            geometry = new Geometry(),
            scale = ( json.scale !== undefined ) ? 1.0 / json.scale : 1.0;
        geometry["vertices"] = [];
        geometry["faces"] = [];
        geometry["faceVertexUvs"] = [];

        parseModel( scale );

        parseSkin();
        parseMorphing( scale );

       // geometry.computeFaceNormals();
       // geometry.computeBoundingSphere();


        function parseModel( scale ) {

            function isBitSet( value, position ) {

                return value & ( 1 << position );

            }

            var i, j, fi,

                offset, zLength,

                colorIndex, normalIndex, uvIndex, materialIndex,

                type,
                isQuad,
                hasMaterial,
                hasFaceVertexUv,
                hasFaceNormal, hasFaceVertexNormal,
                hasFaceColor, hasFaceVertexColor,

                vertex, face, faceA, faceB, color, hex, normal,

                uvLayer, uv, u, v,

                faces = json.faces,
                vertices = json.vertices,
                normals = json.normals,
                colors = json.colors,

                nUvLayers = 0;

            if ( json.uvs !== undefined ) {

                // disregard empty arrays

                for ( i = 0; i < json.uvs.length; i ++ ) {

                    if ( json.uvs[ i ].length ) nUvLayers ++;

                }

                for ( i = 0; i < nUvLayers; i ++ ) {

                    geometry["faceVertexUvs"][ i ] = [];

                }

            }

            offset = 0;
            zLength = vertices.length;

            while ( offset < zLength ) {

                vertex = new Vec3();

                vertex.x = vertices[ offset ++ ] * scale;
                vertex.y = vertices[ offset ++ ] * scale;
                vertex.z = vertices[ offset ++ ] * scale;

                geometry.vertices.push( vertex );

            }

            offset = 0;
            zLength = faces.length;

            while ( offset < zLength ) {

                type = faces[ offset ++ ];


                isQuad              = isBitSet( type, 0 );
                hasMaterial         = isBitSet( type, 1 );
                hasFaceVertexUv     = isBitSet( type, 3 );
                hasFaceNormal       = isBitSet( type, 4 );
                hasFaceVertexNormal = isBitSet( type, 5 );
                hasFaceColor	     = isBitSet( type, 6 );
                hasFaceVertexColor  = isBitSet( type, 7 );

                // console.log("type", type, "bits", isQuad, hasMaterial, hasFaceVertexUv, hasFaceNormal, hasFaceVertexNormal, hasFaceColor, hasFaceVertexColor);

                if ( isQuad ) {

                    faceA = new Face3();
                    faceA.a = faces[ offset ];
                    faceA.b = faces[ offset + 1 ];
                    faceA.c = faces[ offset + 3 ];

                    faceB = new Face3();
                    faceB.a = faces[ offset + 1 ];
                    faceB.b = faces[ offset + 2 ];
                    faceB.c = faces[ offset + 3 ];

                    offset += 4;

                    if ( hasMaterial ) {

                        materialIndex = faces[ offset ++ ];
                        faceA.materialIndex = materialIndex;
                        faceB.materialIndex = materialIndex;

                    }

                    // to get face <=> uv index correspondence

                    fi = geometry.faces.length;

                    if ( hasFaceVertexUv ) {

                        for ( i = 0; i < nUvLayers; i ++ ) {

                            uvLayer = json.uvs[ i ];

                            geometry.faceVertexUvs[ i ][ fi ] = [];
                            geometry.faceVertexUvs[ i ][ fi + 1 ] = []

                            for ( j = 0; j < 4; j ++ ) {

                                uvIndex = faces[ offset ++ ];

                                u = uvLayer[ uvIndex * 2 ];
                                v = uvLayer[ uvIndex * 2 + 1 ];

                                uv = new Vec2( u, v );

                                if ( j !== 2 ) geometry.faceVertexUvs[ i ][ fi ].push( uv );
                                if ( j !== 0 ) geometry.faceVertexUvs[ i ][ fi + 1 ].push( uv );

                            }

                        }

                    }

                    if ( hasFaceNormal ) {

                        normalIndex = faces[ offset ++ ] * 3;

                        faceA.normal.set(
                            normals[ normalIndex ++ ],
                            normals[ normalIndex ++ ],
                            normals[ normalIndex ]
                        );

                        faceB.normal.copy( faceA.normal );

                    }

                    if ( hasFaceVertexNormal ) {

                        for ( i = 0; i < 4; i ++ ) {

                            normalIndex = faces[ offset ++ ] * 3;

                            normal = new Vec3(
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ]
                            );


                            if ( i !== 2 ) faceA.vertexNormals.push( normal );
                            if ( i !== 0 ) faceB.vertexNormals.push( normal );

                        }

                    }


                    if ( hasFaceColor ) {

                        colorIndex = faces[ offset ++ ];
                        hex = colors[ colorIndex ];

                        faceA.color.setHex( hex );
                        faceB.color.setHex( hex );

                    }


                    if ( hasFaceVertexColor ) {

                        for ( i = 0; i < 4; i ++ ) {

                            colorIndex = faces[ offset ++ ];
                            hex = colors[ colorIndex ];

                            if ( i !== 2 ) faceA.vertexColors.push( new Color.fromHex( hex ) );
                            if ( i !== 0 ) faceB.vertexColors.push( new Color.fromHex( hex ) );

                        }

                    }

                    geometry.faces.push( faceA );
                    geometry.faces.push( faceB );

                } else {

                    face = new Face3();
                    face.a = faces[ offset ++ ];
                    face.b = faces[ offset ++ ];
                    face.c = faces[ offset ++ ];

                    if ( hasMaterial ) {

                        materialIndex = faces[ offset ++ ];
                        face.materialIndex = materialIndex;

                    }

                    // to get face <=> uv index correspondence

                    fi = geometry.faces.length;

                    if ( hasFaceVertexUv ) {

                        for ( i = 0; i < nUvLayers; i ++ ) {

                            uvLayer = json.uvs[ i ];

                            geometry.faceVertexUvs[ i ][ fi ] = [];

                            for ( j = 0; j < 3; j ++ ) {

                                uvIndex = faces[ offset ++ ];

                                u = uvLayer[ uvIndex * 2 ];
                                v = uvLayer[ uvIndex * 2 + 1 ];

                                uv = new Vec2( u, v );

                                geometry.faceVertexUvs[ i ][ fi ].push( uv );

                            }

                        }

                    }

                    if ( hasFaceNormal ) {

                        normalIndex = faces[ offset ++ ] * 3;

                        face.normal.set(
                            normals[ normalIndex ++ ],
                            normals[ normalIndex ++ ],
                            normals[ normalIndex ]
                        );

                    }

                    if ( hasFaceVertexNormal ) {

                        for ( i = 0; i < 3; i ++ ) {

                            normalIndex = faces[ offset ++ ] * 3;

                            normal = new Vec3(
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ]
                            );

                            face.vertexNormals.push( normal );

                        }

                    }


                    if ( hasFaceColor ) {

                        colorIndex = faces[ offset ++ ];
                        face.color.setHex( colors[ colorIndex ] );

                    }


                    if ( hasFaceVertexColor ) {

                        for ( i = 0; i < 3; i ++ ) {

                            colorIndex = faces[ offset ++ ];
                            face.vertexColors.push( new Color( colors[ colorIndex ] ) );

                        }

                    }

                    geometry.faces.push( face );

                }

            }

        };

        function parseSkin() {
            var influencesPerVertex = ( json.influencesPerVertex !== undefined ) ? json.influencesPerVertex : 2;

            if ( json.skinWeights ) {

                for ( var i = 0, l = json.skinWeights.length; i < l; i += influencesPerVertex ) {

                    var x =                               json.skinWeights[ i     ];
                    var y = ( influencesPerVertex > 1 ) ? json.skinWeights[ i + 1 ] : 0;
                    var z = ( influencesPerVertex > 2 ) ? json.skinWeights[ i + 2 ] : 0;
                    var w = ( influencesPerVertex > 3 ) ? json.skinWeights[ i + 3 ] : 0;

                    geometry.skinWeights.push( new Vec4( x, y, z, w ) );

                }

            }

            if ( json.skinIndices ) {

                for ( var i = 0, l = json.skinIndices.length; i < l; i += influencesPerVertex ) {

                    var a =                               json.skinIndices[ i     ];
                    var b = ( influencesPerVertex > 1 ) ? json.skinIndices[ i + 1 ] : 0;
                    var c = ( influencesPerVertex > 2 ) ? json.skinIndices[ i + 2 ] : 0;
                    var d = ( influencesPerVertex > 3 ) ? json.skinIndices[ i + 3 ] : 0;

                    geometry.skinIndices.push( new Vec4( a, b, c, d ) );

                }

            }

            geometry.bones = json.bones;

            if ( geometry.bones && geometry.bones.length > 0 && ( geometry.skinWeights.length !== geometry.skinIndices.length || geometry.skinIndices.length !== geometry.vertices.length ) ) {

                console.warn( 'When skinning, number of vertices (' + geometry.vertices.length + '), skinIndices (' +
                geometry.skinIndices.length + '), and skinWeights (' + geometry.skinWeights.length + ') should match.' );

            }


            // could change this to json.animations[0] or remove completely

            geometry.animation = json.animation;
            geometry.animations = json.animations;

        };

        function parseMorphing( scale ) {

            if ( json.morphTargets !== undefined ) {

                var i, l, v, vl, dstVertices, srcVertices;

                for ( i = 0, l = json.morphTargets.length; i < l; i ++ ) {

                    geometry.morphTargets[ i ] = {};
                    geometry.morphTargets[ i ].name = json.morphTargets[ i ].name;
                    geometry.morphTargets[ i ].vertices = [];

                    dstVertices = geometry.morphTargets[ i ].vertices;
                    srcVertices = json.morphTargets [ i ].vertices;

                    for ( v = 0, vl = srcVertices.length; v < vl; v += 3 ) {

                        var vertex = new Vec3();
                        vertex.x = srcVertices[ v ] * scale;
                        vertex.y = srcVertices[ v + 1 ] * scale;
                        vertex.z = srcVertices[ v + 2 ] * scale;

                        dstVertices.push( vertex );

                    }

                }

            }

            if ( json.morphColors !== undefined ) {

                var i, l, c, cl, dstColors, srcColors, color;

                for ( i = 0, l = json.morphColors.length; i < l; i ++ ) {

                    geometry.morphColors[ i ] = {};
                    geometry.morphColors[ i ].name = json.morphColors[ i ].name;
                    geometry.morphColors[ i ].colors = [];

                    dstColors = geometry.morphColors[ i ].colors;
                    srcColors = json.morphColors [ i ].colors;

                    for ( c = 0, cl = srcColors.length; c < cl; c += 3 ) {

                        color = new Color.fromHex( 0xffaa00 );
                        color.setRGB( srcColors[ c ], srcColors[ c + 1 ], srcColors[ c + 2 ] );
                        dstColors.push( color );

                    }

                }

            }

        };

        return { geometry: geometry };


        if ( json.materials === undefined || json.materials.length === 0 ) {

            return { geometry: geometry };

        } else {


            /*
             var materials = this.initMaterials( json.materials, texturePath );

             if ( this.needsTangents( materials ) ) {

             geometry.computeTangents();

             }

             return { geometry: geometry, materials: materials };
             */

        }
    }
};


module.exports = ObjLoder;
