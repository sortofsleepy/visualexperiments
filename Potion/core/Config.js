/**
 * Simple file to hold a couple config variables
 * @type {{shaderPath: string}}
 */
var Config = {
    shaderPath:"../shaders/"
};

export default Config;