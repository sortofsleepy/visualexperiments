
var type = /(canvas|webgl)/.test(url.type) ? url.type : 'svg';
var two = new Two({
    type: Two.Types[type],
    fullscreen: true,
    autostart: true
}).appendTo(document.body);


var background = two.makeRectangle(two.width / 2, two.height / 2, two.width, two.height);
background.noStroke();
background.fill = 'rgb(255, 255, 175)';
background.name = 'background';

var container = two.makeGroup(background);


