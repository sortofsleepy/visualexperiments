package peasey.base;
#if js
    import js.Browser.*;
    import js.html.*;
#end
class Renderer{
    #if js
        public var domElement:Element;    
    #end
       
    #if js
    public function new(?element:Element){
       if(element == null){
            domElement = document.body;   
       }else{
            domElement = element;   
       }
    
    }
    #else
    public function new(){}
    #end
        
 
 
    public function requestAnimationFrame(method:Dynamic){
        var requestAnimationFrame:Dynamic = untyped window.requestAnimationFrame;
        if(requestAnimationFrame == null){
            requestAnimationFrame = function(method:Dynamic):Void{
                untyped __js__("window.setTimeout")(method,1000/60);   
            }
        }
        untyped __js__("requestAnimationFrame")(method);
    }
    
    #if js
    public function apply(obj:Dynamic,options:Dynamic){
        //loop through options and see if we need to do anything else
        for(n in Reflect.fields(options)){
            switch(n){
                case "container":
                        domElement = document.querySelector(Reflect.field(options,n));
                    break;
            }
        }   
    }
    #end
}
