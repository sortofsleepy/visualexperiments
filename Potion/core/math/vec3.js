/**
 *  A simple vector class based/taken off a bunch of different libs out there(but primarily glMatrix)
 *  TODO integrate TypedArrays for Js?
 */
class Vec3{
    constructor(x,y,z){
        this.x = x !== undefined ? x : 0;
        this.y = y !== undefined ? y : 0;
        this.z = z !== undefined ? z : 0;
    }

    clone(){
        return new Vec3(this.x,this.y,this.z);
    }

    /**
     * Copies values from another Vec3 onto this one
     * @param vec the other vector
     * @returns {Vec3} this object for chaining
     */
    copy(vec){
        this.x = vec.x;
        this.y = vec.y;
        this.z = vec.z;

        return this;
    }

    add(vec){
        this.x += vec.x;
        this.y += vec.y;
        this.z += vec.z;
    }

    subtract(vec){
        this.x -= vec.x;
        this.y -= vec.y;
        this.z -= vec.z;

        return this;
    }

    multiply(vec){
        this.x *= vec.x;
        this.y *= vec.y;
        this.z *= vec.z;

        return this;
    }

    divide(vec){
        this.x /= vec.x;
        this.y /= vec.y;
        this.z /= vec.z;

        return this;
    }

    divideScalar(val){
        this.x /= val;
        this.y /= val;
        this.z /= val;

        return this;
    }

    reset(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    distance(vec){
        var x = vec.x - this.x,
            y = vec.y - this.y,
            z = vec.z - this.z;
        return Math.sqrt(x*x + y*y + z*z);
    }

    distanceSquared(vec){
        var x = vec.x - this.x,
            y = vec.y - this.y,
            z = vec.z - this.z;
        return x*x + y*y + z*z;
    }

    normalize(){
        var x = this.x,
            y = this.y,
            z = this.z;
        var len = x*x + y*y + z*z;
        if (len > 0) {
            //TODO: evaluate use of glm_invsqrt here?
            len = 1 / Math.sqrt(len);
            this.x *= len;
            this.y *= len;
            this.z *= len;
        }
        return this;
    }

    lerp(vec,t){
        this.x = this.x + t * (vec.x - this.x);
        this.y = this.y + t * (vec.y - this.y);
        this.z = this.z + t * (vec.z - this.z);

        return this;
    }

    rotateX(vec,rot){
        var p = [], r=[];

        //Translate point to the origin
        p[0] = this.x - vec.x;
        p[1] = this.y - vec.y;
        p[2] = this.z - vec.z;

        //perform rotation
        r[0] = p[0];
        r[1] = p[1]*Math.cos(rot) - p[2]*Math.sin(rot);
        r[2] = p[1]*Math.sin(rot) + p[2]*Math.cos(rot);

        this.x = r[0];
        this.y = r[1];
        this.z = r[2];

        return this;
    }

    rotateY(vec,rot){
        var p = [], r=[];

        //Translate point to the origin
        p[0] = this.x - vec.x;
        p[1] = this.y - vec.y;
        p[2] = this.z - vec.z;

        //perform rotation
        r[0] = p[2]*Math.sin(rot) + p[0]*Math.cos(rot);
        r[1] = p[1];
        r[2] = p[2]*Math.cos(rot) - p[0]*Math.sin(rot);

        this.x = r[0];
        this.y = r[1];
        this.z = r[2];

        return this;
    }

    rotateZ(vec,rot){
        var p = [], r=[];

        //Translate point to the origin
        p[0] = this.x - vec.x;
        p[1] = this.y - vec.y;
        p[2] = this.z - vec.z;

        //perform rotation
        r[0] = p[0]*Math.cos(rot) - p[1]*Math.sin(rot);
        r[1] = p[0]*Math.sin(rot) + p[1]*Math.cos(rot);
        r[2] = p[2];

        this.x = r[0];
        this.y = r[1];
        this.z = r[2];

        return this;
    }

    toString(){
        return 'vec3(' + this.x + ', ' + this.y + ', ' + this.z + ')';
    }
}