var ObjLoader = require("./ObjLoader");
var geom = require("pex-geom");
var glu = require("pex-glu");


var data = ObjLoader.loadModel("/snowflake2.json");
var Geometry = geom.Geometry;
var Mesh = glu.Mesh;

var SnowFlake = function(){
    this.geometry = new Geometry({
        vertices:data.geometry.vertices,
        faces:ObjLoader.stripFaces(data.geometry.faces)
    });



};


SnowFlake.prototype = {

};


module.exports = SnowFlake;
