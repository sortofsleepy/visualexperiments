varying vec2 vUv;

uniform vec3 desired;

#define SPEED 0.5


void main() {
    vec3 _desired = desired;
    vec4 pos = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
	vUv = uv;

    vec3 steer = _desired - pos.xyz;

    steer *= SPEED;

    pos.xyz += steer;

	gl_Position = pos;

}
