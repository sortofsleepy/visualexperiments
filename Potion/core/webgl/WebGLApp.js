import App from "../App"
var shader = require("gl-shader");
var glslify = require("glslify");
var config = require("./../Config");

class WebGLApp extends App{
    constructor(name){
        super("WebGLApp",name);

        //create a context and a domElement
        this.domElement = document.createElement("canvas");
        this.ctx = this.domElement.getContext("webgl");

        this.hasLoadedShader = false;
    }

    /**
     * Sets the size of the canvas element
     * @param width
     * @param height
     */
    setSize(width,height){
        this.domElement.width = width;
        this.domElement.height = height;
    }

    /**
     * Attaches the canvas element to a node on the DOM
     * @param node
     */
    attachTo(node){
        node.appendChild(this.domElement);
    }

    /**
     * Loads a WebGL Shader. It's assumed that both the vertex and fragment are
     * named the same, the difference being the extention, .vert.glsl and .frag.glsl
     * @param shadername
     */
    loadShader(shadername){
        //make sure a default shader is loaded if a name isn't specified.
        shadername = shadername !== undefined ? shadername : "default";

        var createShader = glslify({
            vertex:Config.shaderPath + shadername + "vert.glsl",
            fragment:Config.shaderPath + shadername + "frag.glsl"
        });
        this.program = createShader(this.ctx);

        //indicate that we've loaded a shader
        this.hasLoadedShader = true;
    }

    /**
     * So you can quickly grab the context
     * @returns {WebGLApp.ctx|*}
     */
    gl(){
        return this.ctx;
    }
}

export default WebGLApp;