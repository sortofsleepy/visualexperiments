var BufferEdge = function(vec1,vec2){
    this.v0 = vec1;
    this.v1 = vec2;

};


BufferEdge.prototype = {
    equals:function(other){
        return (this.v0.length() === other.v0.length() && this.v1.length() === other.v1.length());
    },

    inverse:function(){
        return new BufferEdge(this.v1, this.v0);
    }
}