/**
 * The main initialization file.
 * Will take care of setting up the necessary components like a native window(OpenGL) or a browser window(WebGL)
 */

import WebGLApp from "./core/webgl/WebGLApp"

var app = new WebGLApp();