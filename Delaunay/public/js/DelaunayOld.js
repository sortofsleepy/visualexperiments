/**
 * Created by sortofsleepy on 7/23/14.
 * Work of Joshua Bell adapted in a more Object oriented manner for Three.js
 * Reference http://www.travellermap.com/tmp/delaunay.js
 *
 *
 * Steps
 * 1. create plane w/ triangles,
 * 2. insert into this function.
 */


var Delaunay = function(triangles){
    this.triangles = triangles || [];
    this.boundingTriangle = null;

};

Delaunay.prototype = {

    /**
     *  Adds triangles to the stack
     * @param triangles a array of BufferTriangles
     */
    addTriangles:function(triangles){
        this.triangles = triangles;
    },

    /**
     * Does the triangulation
     */
    triangulate:function(){
        //build the geometry of all the triangles and edges
        var len = this.triangles.length;
        for(var i = 0 ; i < len ; ++i){
           this.triangles[i].buildGeometry();
        }




        //compile all the vertices
        this.vertices = [];
        this.built = [];
        var len = this.triangles.length;
        for(var i = 0 ; i < len ; ++i){
            this.vertices.push(this.triangles[i].a.x,this.triangles[i].a.y,this.triangles[i].a.z);
            this.vertices.push(this.triangles[i].b.x,this.triangles[i].b.y,this.triangles[i].b.z);
            this.vertices.push(this.triangles[i].c.x,this.triangles[i].c.y,this.triangles[i].c.z);
        }



        //
        // First, create a "supertriangle" that bounds all vertices
        //
        var st = createBoundingTriangle(this.vertices);

        len = this.vertices.length;
        for(var i = 0;i<len;++i){
            var vec = this.vertices[i];

            this.triangles = this.addVertex(vec,this.triangles);
        }

        //
        // Remove triangles that shared edges with "supertriangle"
        //
        this.triangles = this.triangles.filter(function(triangle) {
            return !(
                triangle.a.x == st.a.x || triangle.a.y == st.b.y || triangle.a.z == st.c.z ||
                triangle.b.x == st.a.x || triangle.b.y == st.b.y || triangle.b.z == st.c.z ||
                triangle.c.x == st.a.x || triangle.c.y == st.b.y || triangle.c.z == st.c.z);
        });

        //loop through and rebuild geometry
        var len = this.triangles.length;



        return this.triangles;

    },

    addVertex:function(vertex,triangles){
        var edges = [];

        // Remove triangles with circumcircles containing the vertex

        triangles = triangles.filter(function(triangle) {
            if (triangle.inCircumcircle(vertex)) {
                edges.push(new BufferEdge(triangle.a, triangle.b,0));
                edges.push(new BufferEdge(triangle.b, triangle.c,0));
                edges.push(new BufferEdge(triangle.c, triangle.a,0));
                return false;
            }

            return true;
        });


        edges = this.uniqueEdges(edges);


        // Create new triangles from the unique edges and new vertex
        edges.forEach(function(edge) {
            triangles.push(new BufferTriangle(edge.v0, edge.v1, vertex));
        });

        return triangles;
    },

    uniqueEdges:function(edges){
        // TODO: This is O(n^2), make it O(n) with a hash or some such
        var uniqueEdges = [];
        for (var i = 0; i < edges.length; ++i) {
            var edge1 = edges[i];
            var unique = true;

            for (var j = 0; j < edges.length; ++j) {
                if (i === j)
                    continue;
                var edge2 = edges[j];
                if (edge1.equals(edge2) || edge1.inverse().equals(edge2)) {
                    unique = false;
                    break;
                }
            }

            if (unique)
                uniqueEdges.push(edge1);
        }

        return uniqueEdges;
    }



};









/**
 * Creates the bounding triangle for the simulation
 * @param vertices array of vertices to use for calculations
 * @returns {BufferTriangle}
 */
function createBoundingTriangle(vertices) {
    // NOTE: There's a bit of a heuristic here. If the bounding triangle
    // is too large and you see overflow/underflow errors. If it is too small
    // you end up with a non-convex hull.

    var minx, miny, maxx, maxy;


    var len = vertices.length;

    for(var i = 0;i<len;++i){
        if (minx === undefined || vertices[i].x < minx) { minx = vertices[i].x; }
        if (miny === undefined || vertices[i].y < miny) { miny = vertices[i].y; }
        if (maxx === undefined || vertices[i].x > maxx) { maxx = vertices[i].x; }
        if (maxy === undefined || vertices[i].y > maxy) { maxy = vertices[i].y; }
    }



    var dx = (maxx - minx) * 10;
    var dy = (maxy - miny) * 10;

    var stv0 = new THREE.Vector3(minx - dx, miny - dy * 3,0);
    var stv1 = new THREE.Vector3(minx - dx, maxy + dy,0);
    var stv2 = new THREE.Vector3(maxx + dx * 3, maxy + dy,0);

    return new BufferTriangle(stv0, stv1, stv2);
}