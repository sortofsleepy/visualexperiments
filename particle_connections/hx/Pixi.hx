
class Pixi{
    static public function Renderer(width:Int,height:Int,?options:Dynamic){
        return untyped __js__("new PIXI.autoDetectRenderer")(width,height,options);
    }
    
    static public function Stage(?color:Dynamic){
        return untyped __js__("new PIXI.Stage")(color);
    }
}