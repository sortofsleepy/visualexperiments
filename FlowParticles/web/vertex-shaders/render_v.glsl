precision mediump float;
precision mediump int;

uniform float time;
attribute vec4 color;

varying vec3 vPosition;
varying vec4 vColor;

void main()	{

	vPosition = position;
	vColor = color;
    vec4 pos = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

	gl_Position = pos;

}
