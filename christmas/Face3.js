/**
 * @author mrdoob / http://mrdoob.com/
 * @author alteredq / http://alteredqualia.com/
 */
var geom = require("pex-geom");
var color = require("pex-color");

var Color = color.Color;
var Vec3 = geom.Vec3;
var Vec2 = geom.Vec2;
var Vec4 = geom.Vec4;
var Face3 = function ( a, b, c, normal, color, materialIndex ) {

    this.a = a;
    this.b = b;
    this.c = c;

    this.normal = normal instanceof Vec3 ? normal : new Vec3();
    this.vertexNormals = normal instanceof Array ? normal : [];

    this.color = color instanceof Color ? color : new Color();
    this.vertexColors = color instanceof Array ? color : [];

    this.vertexTangents = [];

    this.materialIndex = materialIndex !== undefined ? materialIndex : 0;

};

Face3.prototype = {

    constructor: Face3,

    clone: function () {

        var face = new Face3( this.a, this.b, this.c );

        face.normal.copy( this.normal );
        face.color.copy( this.color );

        face.materialIndex = this.materialIndex;

        for ( var i = 0, il = this.vertexNormals.length; i < il; i ++ ) {

            face.vertexNormals[ i ] = this.vertexNormals[ i ].clone();

        }

        for ( var i = 0, il = this.vertexColors.length; i < il; i ++ ) {

            face.vertexColors[ i ] = this.vertexColors[ i ].clone();

        }

        for ( var i = 0, il = this.vertexTangents.length; i < il; i ++ ) {

            face.vertexTangents[ i ] = this.vertexTangents[ i ].clone();

        }

        return face;

    }

};

module.exports = Face3;
