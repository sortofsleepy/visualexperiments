var gulp = require("gulp");
var gutil = require("gulp-util");
var sourcemaps = require("gulp-sourcemaps");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var watchify = require("watchify");
var browserify = require("browserify");
var to5 = require("6to5ify")
var browserSync = require("browser-sync");

var bundler = watchify(browserify('./Main.js',watchify.args));
bundler.transform(to5);

/**==================== JAVASCRIPT ====================*/
gulp.task('js',bundle);

bundler.on('update',bundle);

/**
 * Bundles JS together
 * @returns {*}
 */
function bundle(){
    browserSync.reload();
    return bundler.bundle()
        .on("error",gutil.log.bind(gutil,"Browserify Error"))
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps:true}))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./build"));
}

gulp.task("default",["js"],function(){
    browserSync({
        server:{
            baseDir: "./build"
        }
    });

});