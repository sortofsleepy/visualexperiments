

class WebGLProgram{
    /**
     * Constructor
     * @param context WebGL context
     * @param vertex vertex path or source, make sure to add a "#pragma" so it can be detected as a source
     * @param fragment fragament path.
     */
    constructor(context,vertex,fragment){
        this.gl = context;
        this.program = context.createProgram();

        var vert = vertex !== undefined ? vertex : false;
        var frag = fragment != undefined ? fragment : false;

        if(vert !== false && frag !== false){
            this._init(vert,frag);
        }
    }

    /**
     * Begins construction of a program.
     * @param vertex vertex path or source
     * @param fragment
     */
    _init(vertex,fragment){
        var gl = this.gl;

        //create program object
        var program = gl.createProgram();

        //compile shaders
        var vshader = this._createShader(vertex,gl.VERTEX_SHADER);
        var fshader = this._createShader(fragment,gl.FRAGMENT_SHADER);

        //attach new shaders
        gl.attachShader(program,vshader);
        gl.attachShader(program,fshader);

        //delete the shader objects
        gl.deleteShader(vshader);
        gl.deleteShader(fshader);

        //attempt to link the program
        gl.linkProgram(program);

        if(!gl.getProgramParameter(program,gl.LINK_STATUS)){
            alert( "ERROR:\n" +
            "VALIDATE_STATUS: " + gl.getProgramParameter( program, gl.VALIDATE_STATUS ) + "\n" +
            "ERROR: " + gl.getError() + "\n\n" +
            "- Vertex Shader -\n" + vertex + "\n\n" +
            "- Fragment Shader -\n" + fragment );
            return null;
        }

        this.program = program;
    }

    _createShader(source,type){
        var gl = this.gl;
        var shader = gl.createShader(type);
        gl.shaderSource(shader,source);
        gl.compileShader(shader);

        if(!gl.getShaderParameter(shader,gl.COMPILE_STATUS)){
            alert( ( type == gl.VERTEX_SHADER ? "VERTEX" : "FRAGMENT" ) + " SHADER:\n" + gl.getShaderInfoLog( shader ) );
            return null;
        }

        return shader;
    }

    check(vertex){

    }
}