/**
 * A particle system of triangles
 * @param vec{THREE.Vector} a THREE.Vector marking the boundries of the system
 * @constructor
 */
var Delaunay = function(vec){
    this.triangles = [];

    this.boundingTriangle = null;
    this.vertices = [];


    this.angle = 0;
};


Delaunay.prototype = {

    setVertices:function(num){
        var w = 500;
        var h = 500;
        var inset = 25;
        this.initVerts = [];
        for(var i = 0;i<num;++i){
            this.initVerts.push(new THREE.Vector3(Math.random() * (w - 2 * inset) + inset, Math.random() * (h - 2 * inset) + inset,0));
        }
    },
    addTriangles:function(num){
        var w = 400;
        var h = 400;
        var inset = 25;

        for(var i = 0;i<num;++i){
            this.triangles.push(new BufferTriangle(
                    [new THREE.Vector3( Math.random() * (w - 2 * inset) + inset, Math.random() * (h - 2 * inset) + inset,0),
                    new THREE.Vector3( Math.random() * (w - 2 * inset) + inset, Math.random() * (h - 2 * inset) + inset,0),
                    new THREE.Vector3( Math.random() * (w - 2 * inset) + inset, Math.random() * (h - 2 * inset) + inset,0)]
            ))
        }
    },

    triangulate:function(){
        var verts = this.initVerts;

        var triangles = [];

       /* //build inintial geometry(){
        for(var i = 0;i<this.triangles.length;++i){
        this.triangles[i].buildGeometry();
        }

        //decompile all the Vector3s into individual verts
        for(var i = 0;i<this.triangles.length;++i){
        verts.push(this.triangles[i].vec1,this.triangles[i].vec1,this.triangles[i].vec1);
        verts.push(this.triangles[i].vec2,this.triangles[i].vec2,this.triangles[i].vec2);
        verts.push(this.triangles[i].vec3,this.triangles[i].vec3,this.triangles[i].vec3);
        }*/

        var st = this.computeBoundingTriangle(verts);
        triangles.push(st);
        var obj = this;
        verts.forEach(function(vertex) {
            // NOTE: This is O(n^2) - can be optimized by sorting vertices
            // along the x-axis and only considering triangles that have
            // potentially overlapping circumcircles
            triangles = obj.addVertex(vertex, triangles);
        });

        triangles = triangles.filter(function(triangle) {
            var test = !(
                triangle.vec1.length() == st.vec1.length() || triangle.vec1.length() == st.vec2.length() || triangle.vec1.length() == st.vec3.length() ||
                triangle.vec2.length() == st.vec1.length() || triangle.vec2.length() == st.vec2.length() || triangle.vec2.length() == st.vec3.length() ||
                triangle.vec3.length() == st.vec1.length() || triangle.vec3.length() == st.vec2.length() || triangle.vec3.length() == st.vec3.length());

            console.log(test);
            return test;
        });
        console.log(triangles);
        this.triangles = triangles;

        for(var i = 0;i<this.triangles.length;++i){
            this.triangles[i].buildGeometry();
        }
        return triangles;
    },



    addVertex:function(vertex,triangles){
        var edges = [];

        // Remove triangles with circumcircles containing the vertex

        triangles = triangles.filter(function(triangle) {
            if (triangle.inCircumcircle(vertex)) {
                edges.push(new BufferEdge(triangle.vec1, triangle.vec2,0));
                edges.push(new BufferEdge(triangle.vec2, triangle.vec3,0));
                edges.push(new BufferEdge(triangle.vec3, triangle.vec1,0));
                return false;
            }

            return true;
        });


        edges = this.uniqueEdges(edges);


        // Create new triangles from the unique edges and new vertex
        edges.forEach(function(edge) {
            triangles.push(new BufferTriangle([edge.v0, edge.v1, vertex]));
        });

        return triangles;
    },

    uniqueEdges:function(edges){
        // TODO: This is O(n^2), make it O(n) with a hash or some such
        var uniqueEdges = [];
        for (var i = 0; i < edges.length; ++i) {
            var edge1 = edges[i];
            var unique = true;

            for (var j = 0; j < edges.length; ++j) {
                if (i === j)
                    continue;
                var edge2 = edges[j];
                if (edge1.equals(edge2) || edge1.inverse().equals(edge2)) {
                    unique = false;
                    break;
                }
            }

            if (unique)
                uniqueEdges.push(edge1);
        }

        return uniqueEdges;
    },



    computeBoundingTriangle:function(vertices){
        // NOTE: There's a bit of a heuristic here. If the bounding triangle
        // is too large and you see overflow/underflow errors. If it is too small
        // you end up with a non-convex hull.

        var minx, miny, maxx, maxy;


        var len = vertices.length;

        for(var i = 0;i<len;++i){
            if (minx === undefined || vertices[i].x < minx) { minx = vertices[i].x; }
            if (miny === undefined || vertices[i].y < miny) { miny = vertices[i].y; }
            if (maxx === undefined || vertices[i].x > maxx) { maxx = vertices[i].x; }
            if (maxy === undefined || vertices[i].y > maxy) { maxy = vertices[i].y; }
        }



        var dx = (maxx - minx) * 10;
        var dy = (maxy - miny) * 10;

        var stv0 = new THREE.Vector3(minx - dx, miny - dy * 3,0);
        var stv1 = new THREE.Vector3(minx - dx, maxy + dy,0);
        var stv2 = new THREE.Vector3(maxx + dx * 3, maxy + dy,0);

        return new BufferTriangle([stv0, stv1, stv2]);
    },

    update:function(){
        for(var i = 0;i<this.triangles.length;++i){
            this.triangles[i].update();
        }
    }
};