module.exports = function(grunt){
  var fs = require("fs");
  var exec = require("child_process").exec;


  //base directory that site goes into
  var base = __dirname + "/public/";

  //base javascript directory
  var jsdir = base + "js/";

  var command = "cd " + __dirname + "/hx && haxe -main Runner -js " + __dirname + "/public/js/App.js";

  grunt.initConfig({
    pkg:grunt.file.readJSON("package.json"),
    shell:{

      target:{
        failOnError:false,
        command:command
      }
    },
    compass: {
      dist: {
        options: {
          basePath:__dirname + "/public",
          sassDir:"scss",
          cssDir: "css"
        }

      }
    },
    watch: {
      options: {
        livereload: true
      },


      scripts:{
        files:["hx/**/*.hx"],
        tasks:["shell"]
      },

      compass:{
        files:[
          __dirname +"/public/scss/*.scss",
          __dirname + "/public/scss/partials/*.scss"
        ],
        tasks:['compass:dist']
      }

    }
  })


  grunt.loadNpmTasks( 'grunt-contrib-watch' );
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-shell');

  //builds everything
  grunt.registerTask( 'default', ['compass:dist']);



};//end gruntfile
