#include "cinder/app/AppNative.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "ParticleSystem.h"
#include "Typography.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class LetteringApp : public AppNative {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
    void prepareSettings(Settings * settings);
	void update() override;
	void draw() override;
    
    ParticleSystem system;
    xoio::Typography type;
};

void LetteringApp::prepareSettings(cinder::app::AppBasic::Settings *settings){
    settings->setWindowSize(1024, 768);
}

void LetteringApp::setup()
{
    system.addParticle(9000);
    system.init();
    
   // system.setShader("particle.vert.glsl", "particle.frag.glsl");
}

void LetteringApp::mouseDown( MouseEvent event )
{
}

void LetteringApp::update()
{
    system.updateBuffers();
}

void LetteringApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
    system.draw();
}

CINDER_APP_NATIVE( LetteringApp, RendererGl )
