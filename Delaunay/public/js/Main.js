
Math.randomInRange=function(t,e){return t+(e-t)*Math.random()};
Math.PIM2=2*Math.PI;

var renderer = new THREE.WebGLRenderer();
var camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1000 );
camera.position.z = 300;
camera.position.x = 300;
camera.position.y = 300;
var scene = new THREE.Scene();
var stats = "";

var system = new Delaunay();
 system.setVertices(600);
 system.triangulate();





stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.top = '0px';
document.getElementsByTagName("body")[0].appendChild( stats.domElement );



renderer.setSize(window.innerWidth,window.innerHeight);
document.getElementsByTagName("body")[0].appendChild(renderer.domElement);

draw();
function draw(){
    window.requestAnimationFrame(draw);
    renderer.render(scene,camera);

    system.update();

    stats.update();


}

