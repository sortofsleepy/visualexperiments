/**
 * The base specification of a application
 */
class App{
    /**
     * Constructor
     * @param type the type of application - should either be "WebGL" or "OpenGL"
     * @param name the name of the application. Becomes the <title> on a WebGL page and the window name of a opengl page
     */
    constructor(type,name){
        //if type isn't specified, we create a WebGL automatically
        if((type !== "WebGL") || (type !== "OpeNGL")){
            this.type = "WebGL";
        }else{
            this.type = type;
        }
        this.name = name !== undefined ? name : "Potion Application";
    }
}

export default App;